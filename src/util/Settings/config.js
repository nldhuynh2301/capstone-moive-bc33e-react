import axios from "axios";

export const DOMAIN = "https://movienew.cybersoft.edu.vn";

export const TOKEN =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzM0UiLCJIZXRIYW5TdHJpbmciOiIyNi8wNC8yMDIzIiwiSGV0SGFuVGltZSI6IjE2ODI0NjcyMDAwMDAiLCJuYmYiOjE2NTQzNjIwMDAsImV4cCI6MTY4MjYxNDgwMH0.GGqFf8-ZXIqAjnaJZ40LjQvUHb1VyvRv3XtEIsMe_qE";
export const configHeaders = () => {
  return {
    TokenCybersoft: TOKEN,
  };
};
export const GROUP_ID = "GP03";
export const BAT_LOADING = "BAT_LOADING";
export const TAT_LOADING = "TAT_LOADING";
export const USER_LOGIN = "USER_LOGIN";
export const USER_REGISTER = "USER_REGISTER";

export const SET_FILM_SAP_CHIEU = "SET_FILM_SAP_CHIEU";
export const SET_FILM_DANG_CHIEU = "SET_FILM_DANG_CHIEU";

export const https = axios.create({
  baseURL: DOMAIN,
  headers: configHeaders(),
});
// https.interceptors.request.use(
//   function (config) {
//     store.dispatch({
//       type: BAT_LOADING,
//     });

//     return config;
//   },
//   function (error) {
//     return Promise.reject(error);
//   }
// );

// // Add a response interceptor
// https.interceptors.response.use(
//   function (response) {
//     console.log("after AXIOS");
//     store.dispatch({
//       type: TAT_LOADING,
//     });

//     return response;
//   },
//   function (error) {
//     return Promise.reject(error);
//   }
// );
