import logo from "./logo.svg";
import "./App.css";
import { createBrowserHistory } from "history";
import { Route, Router, Switch } from "react-router-dom";

import Home from "./pages/Home/Home";
import { HomeTemplate } from "./templates/HomeTemplate/HomeTemplate";
import Contact from "./pages/Contact/Contact";
import News from "./pages/News/News";
import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";
import Detail from "./pages/Detail/Detail";
import Profile from "./pages/Profile/Profile";
// import { UserTemplate } from "./templates/UserTemplate/UserTemplate";
import { UserTemplate } from "./templates/UserTemplate/UserTemplate";
export const history = createBrowserHistory();
function App() {
  return (
    <Router history={history}>
      <Switch>
        <UserTemplate path="/login" exact Component={Login} />
        <HomeTemplate path="/home" exact Component={Home} />
        <HomeTemplate path="/contact" exact Component={Contact} />
        <HomeTemplate path="/news" exact Component={News} />
        <HomeTemplate path="/detail/:id" exact Component={Detail} />
        <HomeTemplate path="/profile" exact Component={Profile} />
        <UserTemplate path="/register" exact Component={Register} />
        <Route path="/login" exact Component={Login} />
        <Route path="/register" exact Component={Register} />
        <HomeTemplate path="/" exact Component={Home} />
      </Switch>
    </Router>
  );
}

export default App;
