import React from "react";
import { PlayCircleOutlined } from "@ant-design/icons";
import "../Film/Film_Flip.css";
import { Button } from "antd";
import "../../util/button.css";
import { NavLink } from "react-router-dom";
import { history } from "../../App";
import ReactPlayer from "react-player";

export default function Film_Flip(props) {
  const { item } = props;
  console.log("item.trailer333", item.trailer);
  return (
    <div>
      <div
        className="flip-card mt-2 px-6 py-5"
        style={{
          borderRadius: "20px",
        }}
      >
        <div className="flip-card-inner ">
          <div className="flip-card-front ">
            <div
              style={{
                background: `url(${item.hinhAnh})`,
                backgroundSize: "100%",
                height: "260px",
                //   borderRadius: "20px",
              }}
              className=" bg-center bg-no-repeat bg-cover rounded-2xl"
            >
              <img
                src={item.hinhAnh}
                alt={item.tenPhim}
                style={{ width: 150, height: 260 }}
                className=" opacity-0"
              />
            </div>
          </div>
          <div className="flip-card-back" style={{ position: "relative" }}>
            <div className="rounded-2xl text-center">
              <div
                style={{
                  position: "absolute",
                  top: 0,
                  left: 0,
                  textAlign: "center",
                  alignItems: "center",
                }}
              >
                <h1 className="title-font sm:text-2xl text-xl font-medium text-gray-900 mb-3 text-center">
                  {item.tenPhim}
                </h1>
              </div>
              <div
                className="w-full h-full mt-5 text-center"
                style={{
                  position: "absolute",
                  //   backgroundColor: "rgba(0,0,0,.5)",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  textAlign: "center",
                }}
              >
                <div>
                  <div className="rounded-full cursor-pointer">
                    <NavLink to={`/detail/${item.maPhim}`}>
                      <PlayCircleOutlined
                        style={{ fontSize: "50px", color: "red" }}
                      />
                      <div className="text-green-500">
                        Click vào đây để xem chi tiết
                      </div>
                    </NavLink>
                  </div>
                  <div className="align-item-center leading-relaxed mt-30 text-center m-1 p-1">
                    {item.moTa.substr(0, 90) + "..."}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        onClick={() => {
          history.push(`/detail/${item.maPhim}`);
        }}
        className="relative w-20 mt-30 btn-grad text-center cursor-pointer py-2 bg-indigo-300 text-success-50 font-bold text-white"
      >
        Đặt vé
      </div>
    </div>
  );
}
