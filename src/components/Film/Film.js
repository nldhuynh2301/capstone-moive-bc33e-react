import { Button } from "antd";
import React from "react";

export default function Film(props) {
  const { item } = props;
  return (
    <div className="mr-2 h-full bg-gray-100 bg-opacity-75 px-8 pt-16 pb-24 rounded-lg overflow-hidden text-center relative">
      <div
        style={{ background: `url(${item.hinhAnh})`, backgroundSize: "100%" }}
        className="rounded-lg bg-center h-100 w-150 bg-no-repeat bg-cover"
      >
        <img
          src={item.hinhAnh}
          alt={item.tenPhim}
          className="rounded-lg h-100 w-150 opacity-0 "
        />
      </div>

      <h1 className="title-font sm:text-2xl text-xl font-medium text-gray-900 mb-3">
        {item.tenPhim}
      </h1>
      <p className="leading-relaxed mb-3">{item.moTa}</p>
      <Button className="text-white-500 inline-flex items-center bg-purple-400">
        Đặt vé
        <svg
          className="w-4 h-4 ml-2"
          viewBox="0 0 24 24"
          stroke="currentColor"
          strokeWidth={2}
          fill="none"
          strokeLinecap="round"
          strokeLinejoin="round"
        >
          <path d="M5 12h14" />
          <path d="M12 5l7 7-7 7" />
        </svg>
      </Button>
    </div>
  );
}
