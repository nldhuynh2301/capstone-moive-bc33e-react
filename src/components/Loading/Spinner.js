import React, { useState } from "react";
import { useSelector } from "react-redux";
import { HashLoader } from "react-spinners";
export default function Spinner() {
  let { isLoading } = useSelector((state) => {
    return state.spinnerReducer;
  });
  return isLoading ? (
    <div className="fixed w-screen h-screen flex justify-center items-center bg-red-400/30 top-0 left-0 z-50">
      <HashLoader color="#da2c38" margin={3} size={90} />
    </div>
  ) : (
    <></>
  );
}
