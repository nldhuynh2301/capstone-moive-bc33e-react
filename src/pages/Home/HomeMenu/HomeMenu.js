import React, { Component, Fragment, PureComponent, useState } from "react";
import { Radio, Space, Tabs } from "antd";
import TabPane from "antd/es/tabs/TabPane";
import { Connect } from "react-redux";
import { NavLink } from "react-router-dom";
import moment from "moment/moment";
import "../../../util/button.css";
// export default function HomeMenu(props) {

//   console.log("propsHOME MENU", props);
//   const [tabPosition, setTabPosition] = useState("left");
//   const changeTabPosition = (e) => {
//     setTabPosition(e.target.value);
//   };

// }
// const { TabPane } = Tabs;
export default class HomeMenu extends PureComponent {
  state = {
    tabPosition: "left",
  };
  changeTabPosition = (e) => {
    this.setState({ tabPosition: e.target.value });
  };
  componentDidMount() {}
  renderHeThongRap = () => {
    return this.props.heThongRapChieu?.map((heThongRap, index) => {
      let { tabPosition } = this.state;
      return (
        <TabPane
          tab={
            <img
              src={heThongRap.logo}
              className="w-10 rounded-full border-b-4"
            />
          }
          key={index}
        >
          <Tabs tabPosition={tabPosition}>
            {heThongRap.lstCumRap?.map((cumRap, index) => {
              return (
                <TabPane
                  tab={
                    <div style={{ width: "200px" }}>
                      <img src={cumRap.hinhAnh} className="w-12 rounded-full" />
                      <div className="text-blue-400 ">{cumRap.tenCumRap}</div>
                      <div className="text-black-300 border-b-2 border-indigo-500">
                        Địa chỉ: {cumRap.diaChi.substr(0, 30) + "..."}
                      </div>
                    </div>
                  }
                  key={index}
                >
                  {cumRap.danhSachPhim?.map((phim, index) => {
                    return (
                      <Fragment key={index}>
                        <h1 className="text-green-700 mt-5 text-2xl">
                          {phim.tenPhim}
                        </h1>
                        <div className="flex border-b-2 border-indigo-300">
                          <div className="flex">
                            <img
                              src={phim.hinhAnh}
                              className="w-40 h-40 rounded-lg mt-5 mp-5"
                              alt={phim.tenPhim}
                              onError={(e) => {
                                e.target.onerror = null;
                                e.target.src =
                                  "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQHvaRbcer1ya3BIvzWC73YG3GxuPrQUFNhiJqGRMm74qpAL8km";
                              }}
                            />

                            <div className="grid grid-cols-5 gird-gap-5 my-3 text-center">
                              {phim.lstLichChieuTheoPhim
                                ?.slice(0, 10)
                                .map((lichChieu, index) => {
                                  return (
                                    <NavLink
                                      to="/"
                                      key={index}
                                      className="   p-1 text-center"
                                    >
                                      <p className="flex btn-grad rounded-md mt-5 ml-3 p-2">
                                        {moment(
                                          lichChieu.ngayChieuGioChieu
                                        ).format("dd/mm/yyyy hh:mm AM")}
                                      </p>
                                      {/* <span className="bg-blue-300 m-1 p-1 mt-2 rounded-md"></span> */}
                                    </NavLink>
                                  );
                                })}
                            </div>
                          </div>
                        </div>
                        <br />
                      </Fragment>
                    );
                  })}
                  <div className="border"></div>
                </TabPane>
              );
            })}
          </Tabs>
          ;
        </TabPane>
      );
    });
  };

  render() {
    console.log("PROPS", this.props);

    const { tabPosition } = this.state;
    return (
      <>
        <Tabs tabPosition={tabPosition}>{this.renderHeThongRap()}</Tabs>
      </>
    );
  }
}
