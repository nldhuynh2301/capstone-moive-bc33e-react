import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { layDanhSachPhimAction } from "../../redux/actions/QuanLyPhimAction";
import { layDanhSachRapAction } from "../../redux/actions/QuanLyRapAction";
import HomeCarousel from "../../templates/HomeTemplate/Layout/HomeCarousel/HomeCarousel";

import HomeMenu from "./HomeMenu/HomeMenu";
import MultipleRowSlick from "./SlickFilm/MultipleRowSlick";
//ket noi redux

export default function Home(props) {
  const { arrFilm } = useSelector((state) => state.QuanLyPhimReducer);
  const dispatch = useDispatch();
  const { heThongRapChieu } = useSelector((state) => state.QuanLyRapReducer);
  // const renderFilms = () => {
  //   return arrFilm.map((phim, index) => {
  //     return <Film key={index} />;
  //   });
  // };
  useEffect(() => {
    const action = layDanhSachPhimAction();
    dispatch(action); //dispatch function tu thunk
    dispatch(layDanhSachRapAction());
  }, []);
  return (
    <div
      // style={{
      //   backgroundImage:
      //     "url(https://mcdn.wallpapersafari.com/medium/45/98/4f52lq.jpg)",
      //   backgroundSize: "100%",
      // }}
      // className="bg-center w-full h-full bg-no-repeat bg-cover"
      className=""
    >
      <HomeCarousel />
      <section className="text-gray-600 body-font">
        <div className="px-20 py-30 mt-10 mb-10 mx-auto">
          <MultipleRowSlick arrFilm={arrFilm} />

          {/* <div className="flex flex-wrap -m-4 justify-center">
            {renderFilms()}
          </div> */}
        </div>
      </section>
      <HomeMenu heThongRapChieu={heThongRapChieu} />
    </div>
  );
}
