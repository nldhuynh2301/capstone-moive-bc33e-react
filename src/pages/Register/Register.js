import { useFormik } from "formik";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { dangKyAction } from "../../redux/actions/QuanLyNguoiDungAction";

export default function Register(props) {
  const dispatch = useDispatch();

  const { userRegister } = useSelector((state) => state.QuanLyNguoiDungReducer);

  console.log("userREGISTER", userRegister);

  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDT: "",
      maNhom: "",
      hoTen: "",
    },
    onSubmit: (values) => {
      const action = dangKyAction(values);
      dispatch(action);

      console.log("valuesREGISTER", values);
    },
  });

  return (
    <form
      onSubmit={formik.handleSubmit}
      className="lg:w-1/2 xl:max-w-screen-sm "
    >
      <div className="py-12 bg-indigo-100 lg:bg-white flex justify-center lg:justify-start lg:px-12 text-red-500">
        <div className="cursor-pointer flex items-center">
          <div className="text-2xl text-red-500 tracking-wide ml-2 font-semibold">
            <NavLink
              rel="noopener noreferrer"
              to="/"
              className="flex justify-center space-x-3 md:justify-start text-black-600"
            >
              <div className="flex items-center justify-center w-12 h-12 rounded-full dark:bg-violet-400">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 32 32"
                  fill="currentColor"
                  className="flex-shrink-0 w-5 h-5 rounded-full dark:text-gray-900"
                >
                  <path d="M18.266 26.068l7.839-7.854 4.469 4.479c1.859 1.859 1.859 4.875 0 6.734l-1.104 1.104c-1.859 1.865-4.875 1.865-6.734 0zM30.563 2.531l-1.109-1.104c-1.859-1.859-4.875-1.859-6.734 0l-6.719 6.734-6.734-6.734c-1.859-1.859-4.875-1.859-6.734 0l-1.104 1.104c-1.859 1.859-1.859 4.875 0 6.734l6.734 6.734-6.734 6.734c-1.859 1.859-1.859 4.875 0 6.734l1.104 1.104c1.859 1.859 4.875 1.859 6.734 0l21.307-21.307c1.859-1.859 1.859-4.875 0-6.734z" />
                </svg>
              </div>
              <span className="self-center text-3xl font-semibold text-red-500">
                FULLSTACK FILMS
              </span>
            </NavLink>
          </div>
        </div>
      </div>
      <div className=" px-12 sm:px-24 md:px-48 lg:px-12 lg:mt-16 xl:px-24 xl:max-w-2xl text-red-700">
        <h2
          className="text-center text-2xl text-red-500 font-display font-semibold lg:text-left xl:text-5xl
      xl:text-bold"
        >
          Đăng ký
        </h2>
        <div className="mt-12">
          <div>
            <div>
              <div className="text-sm font-bold  tracking-wide text-red-500">
                Tài khoản
              </div>
              <input
                name="taiKhoan"
                onChange={formik.handleChange}
                className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                placeholder="Nhập vào tài khoản"
              />
            </div>
            <div className="mt-2">
              <div className="flex justify-between items-center">
                <div className="text-sm font-bold  tracking-wide text-red-500">
                  Mật khẩu
                </div>
              </div>
              <input
                type="password"
                name="matKhau"
                onChange={formik.handleChange}
                className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                placeholder="Nhập vào mật khẩu"
              />
            </div>
            <div className="mt-2">
              <div className="text-sm font-bold  tracking-wide text-red-500">
                Email
              </div>
              <input
                name="email"
                onChange={formik.handleChange}
                className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                placeholder="Nhập vào email"
              />
            </div>
            <div className="mt-2">
              <div className="text-sm font-bold  tracking-wide text-red-500">
                Số điện thoại
              </div>
              <input
                name="soDT"
                onChange={formik.handleChange}
                className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                placeholder="Nhập vào số điện thoại"
              />
            </div>
            <div className="mt-2">
              <div className="text-sm font-bold  tracking-wide text-red-500">
                Mã nhóm
              </div>
              <input
                name="maNhom"
                onChange={formik.handleChange}
                className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                placeholder="Nhập vào mã nhóm"
              />
            </div>
            <div className="mt-2">
              <div className="text-sm font-bold  tracking-wide text-red-500">
                Họ tên
              </div>
              <input
                name="hoTen"
                onChange={formik.handleChange}
                className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                placeholder="Nhập vào họ tên"
              />
            </div>
            <div className="mt-5">
              <button
                className="bg-red-700 text-gray-100 p-4 w-full rounded-full tracking-wide
                  font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-red-900
                  shadow-lg"
              >
                Đăng ký
              </button>
            </div>
          </div>
          <div className="mt-12 text-sm font-display font-semibold text-gray-700 text-center">
            Bạn đã có tài khoản?{" "}
            <NavLink
              to="login"
              className="cursor-pointer text-red-600 hover:text-red-800"
            >
              Đăng nhập
            </NavLink>
          </div>
        </div>
      </div>
    </form>
  );
}
