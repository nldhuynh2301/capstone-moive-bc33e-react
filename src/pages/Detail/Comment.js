import React from "react";
import { Button, Comment, Form, Header } from "semantic-ui-react";
const CommentExampleThreaded = () => (
  <Comment.Group threaded>
    <Header as="h3" dividing>
      Bình luận:
    </Header>

    <Comment>
      <Comment.Avatar
        as="a"
        src="https://react.semantic-ui.com/images/avatar/small/matt.jpg"
      />
      <Comment.Content>
        <Comment.Author as="a">Ly Ly</Comment.Author>
        <Comment.Metadata>
          <span>Hôm nay lúc 5:42PM</span>
        </Comment.Metadata>
        <Comment.Text>
          Với khả năng tương tác giúp khán giả hoá thân vào nhân vật Matt, bạn
          sẽ làm gì khi bị cả thế giới quay lưng trong giây phút tính mạng gặp
          nguy hiểm? Tại sao..!!!
        </Comment.Text>
        <Comment.Actions>
          <a>Trả lời</a>
        </Comment.Actions>
      </Comment.Content>
    </Comment>

    <Comment>
      <Comment.Avatar
        as="a"
        src="https://react.semantic-ui.com/images/avatar/small/elliot.jpg"
      />
      <Comment.Content>
        <Comment.Author as="a">Meta Huma</Comment.Author>
        <Comment.Metadata>
          <span>Hôm qua lúc 12:30AM</span>
        </Comment.Metadata>
        <Comment.Text>
          <p>
            Nữ hoàng Ramonda, Shuri, M’Baku, Okoye và Dora Milaje chiến đấu để
            bảo vệ quốc gia của họ khỏi sự can thiệp của các thế lực thế giới
            sau cái chết của Vua T’Challa. Khi người Wakanda cố gắng nắm bắt
            chương tiếp theo của họ, các anh hùng phải hợp tác với nhau với sự
            giúp đỡ của War Dog Nakia và Everett Ross và tạo ra một con đường
            mới cho vương quốc Wakanda. Kết buồn...
          </p>
        </Comment.Text>
        <Comment.Actions>
          <a>
            Nữ hoàng Ramonda, Shuri, M’Baku, Okoye và Dora Milaje chiến đấu để
            bảo vệ quốc gia của họ khỏi sự can thiệp của các thế lực thế giới
            sau cái chết của Vua T’Challa. Khi người Wakanda cố gắng nắm bắt
            chương tiếp theo của họ, các anh hùng phải hợp tác với nhau với sự
            giúp đỡ của War Dog Nakia và Everett Ross và tạo ra một con đường
            mới cho vương quốc Wakanda.
          </a>
        </Comment.Actions>
      </Comment.Content>

      <Comment.Group>
        <Comment>
          <Comment.Avatar
            as="a"
            src="https://react.semantic-ui.com/images/avatar/small/jenny.jpg"
          />
          <Comment.Content>
            <Comment.Author as="a">Jenny Phạm</Comment.Author>
            <Comment.Metadata>
              <span>Vừa xong</span>
            </Comment.Metadata>
            <Comment.Text>Phim hay nhưng vai chính không đẹp :)</Comment.Text>
            <Comment.Actions>
              <a>Trả lời</a>
            </Comment.Actions>
          </Comment.Content>
        </Comment>
      </Comment.Group>
    </Comment>

    <Comment>
      <Comment.Avatar
        as="a"
        src="https://react.semantic-ui.com/images/avatar/small/joe.jpg"
      />
      <Comment.Content>
        <Comment.Author as="a">Hải Thoại</Comment.Author>
        <Comment.Metadata>
          <span>5 ngày trước</span>
        </Comment.Metadata>
        <Comment.Text>Kết buồn nhưng mở, mong có phần sau...</Comment.Text>
        <Comment.Actions>
          <a>Trả lời</a>
        </Comment.Actions>
      </Comment.Content>
    </Comment>

    <Form reply>
      <Form.TextArea />
      <Button content="Add Reply" labelPosition="left" icon="edit" primary />
    </Form>
  </Comment.Group>
);

export default CommentExampleThreaded;
