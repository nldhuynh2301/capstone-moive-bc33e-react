import React from "react";
import ContactTemplate from "../../templates/HomeTemplate/ContactTemplate";
import Header from "../../templates/HomeTemplate/Layout/Header/Header";
export default function Contact() {
  return (
    <div>
      <Header />
      <ContactTemplate />
    </div>
  );
}
