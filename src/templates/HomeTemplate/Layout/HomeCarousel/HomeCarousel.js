import React, { useEffect } from "react";
import { Carousel } from "antd";
import { useDispatch, useSelector } from "react-redux";

import { getCarouselAction } from "../../../../redux/actions/CarouselAction";
const contentStyle = {
  height: "500px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  backgroundPosition: "center",
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
};
export default function HomeCarousel(props) {
  const { arrImg } = useSelector((state) => state.CarouselReducer);
  const dispatch = useDispatch();
  // useEffect(
  //   async () => {
  //   try {
  //     const result = await axios({
  //       url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`,
  //       method: "GET",
  //       headers: configHeaders(),
  //     });
  //     console.log("result", result);
  //     //post to reducer
  //     dispatch({
  //       type: "SET_CAROUSEL",
  //       arrImg: result.data.content,
  //     });
  //   } catch (errors) {
  //     console.log("error", errors);
  //   }
  // }, []);

  //sẽ tự kích hoạt khio component load ra web

  useEffect(() => {
    //1 action = Ơtype:',data}
    //2 (phải cài middleware);
    // callBackFunction(disptach)
    // const action = getCarouselAction(1);
    dispatch(getCarouselAction());
  }, []);
  // console.log("arrImg", arrImg);

  const renderImg = () => {
    return arrImg.map((item, index) => {
      return (
        <div key={index}>
          <div
            style={{ ...contentStyle, backgroundImage: `url(${item.hinhAnh})` }}
          >
            <img
              src={item.hinhAnh}
              className="w-100  bg-cover opacity-0"
              alt="imgCarousel1"
            />
          </div>
        </div>
      );
    });
  };
  return (
    <div>
      <Carousel effect="fade" className="z-1 relative">
        {renderImg()}
      </Carousel>
    </div>
  );
}
