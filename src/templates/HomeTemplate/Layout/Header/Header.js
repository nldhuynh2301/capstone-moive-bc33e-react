import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import { history } from "../../../../App";
import { Select } from "antd";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import _ from "lodash";
import { TOKEN, USER_LOGIN } from "../../../../util/Settings/config";

const { Option } = Select;

export default function Header(props) {
  const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);

  const { t, i18n } = useTranslation();

  const handleChange = (value) => {
    i18n.changeLanguage(value);
  };

  const renderLogin = () => {
    if (_.isEmpty(userLogin)) {
      return (
        <Fragment>
          <button
            onClick={() => {
              history.push("/login");
            }}
            className="text-white self-center px-8 py-3 rounded"
          >
            {t("signin")}
          </button>
          <button
            onClick={() => {
              history.push("/register");
            }}
            className="text-white self-center px-8 py-3 font-semibold rounded dark:bg-violet-400 dark:text-gray-900"
          >
            {t("register")}
          </button>
        </Fragment>
      );
    }

    return (
      <Fragment>
        {" "}
        <button
          onClick={() => {
            history.push("/profile");
          }}
          className="self-center px-8 py-3 rounded text-white"
        >
          <span className="text-blue-400">Tài Khoản:</span> {userLogin.taiKhoan}
        </button>
        <button
          onClick={() => {
            localStorage.removeItem(USER_LOGIN);
            localStorage.removeItem(TOKEN);
            history.push("/home");
            window.location.reload();
          }}
          className="text-yellow-500 mr-5"
        >
          Đăng xuất
        </button>
      </Fragment>
    );
  };
  return (
    <header className="p-4 dark:bg-gray-800 dark:text-gray-100 bg-green-900 bg-opacity-40 text-black fixed w-full z-1  bg-cover bg-center z-10">
      <nav>
        <div className=" flex justify-around h-16">
          <a
            rel="noopener noreferrer"
            href="#"
            aria-label="Back to homepage"
            className="flex items-center p-2"
          >
            <img
              src="https://pbs.twimg.com/profile_images/1418783196099923968/QmqVBSyz_400x400.jpg"
              className=" w-20 rounded-r-full"
              alt="Full Stack Films"
            />
          </a>
          <ul className="items-stretch hidden space-x-3 lg:flex">
            <li className="flex hover:text-black-600">
              <NavLink
                to="/home"
                className=" text-white flex items-center px-4 dark:border-transparent dark:text-violet-400 dark:border-violet-400"
                activeClassName="text-red-400 font-bold border-b-2 border-white"
              >
                Trang chủ
              </NavLink>
            </li>

            <li className="flex">
              <NavLink
                to="/contact"
                className="hover:text-black-600 text-white flex items-center px-4 -mb-1 dark:border-transparent dark:text-violet-400 dark:border-violet-400"
                activeClassName="text-red-400 font-bold border-b-2 border-white"
              >
                Liên hệ
              </NavLink>
            </li>
            <li className="flex">
              <NavLink
                to="/news"
                className="text-white flex items-center px-4 -mb-1 dark:border-transparent dark:text-violet-400 dark:border-violet-400"
                activeClassName="text-red-400 font-bold border-b-2 border-white"
              >
                Tin tức
              </NavLink>
            </li>
            {/* <li className="flex">
            <NavLink
              to="/"
              className="text-white flex items-center px-4 -mb-1 dark:border-transparent dark:text-violet-400 dark:border-violet-400"
              activeClassName="border-b-2 text-blue-700 "
            >
              Ứng dụng
            </NavLink>
          </li> */}
          </ul>

          <div className="items-center flex-shrink-0 hidden lg:flex">
            {renderLogin()}

            {/* <Select
            defaultValue="en"
            style={{ width: 100 }}
            onChange={handleChange}
          >
            <Option value="en">Eng</Option>
            <Option value="chi">Chi</Option>

            <Option value="vi">Vi</Option>
          </Select> */}
          </div>
          {/* <div className="items-center flex-shrink-0 hidden lg:flex">
          <button className="text-white self-center px-8 py-3 rounded">
            Sign in
          </button>
          <button className="text-white self-center px-8 py-3 font-semibold rounded dark:bg-violet-400 dark:text-gray-900">
            Sign up
          </button>
        </div> */}
          <button className="p-4 lg:hidden">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              className="w-6 h-6 dark:text-gray-100"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M4 6h16M4 12h16M4 18h16"
              />
            </svg>
          </button>
        </div>
      </nav>
    </header>
  );
}
