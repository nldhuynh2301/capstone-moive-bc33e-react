import Axios from "axios";
import { configHeaders } from "../templates/URLconfig/tokenCyber";
import { DOMAIN, TOKEN } from "../util/Settings/config";
export class baseService {
  //put json về phía backend
  put = (url, model) => {
    return Axios({
      url: `${DOMAIN}/${url}`,
      method: "PUT",
      data: model,
      headers: configHeaders(), //JWT
    });
  };

  post = (url, model) => {
    return Axios({
      url: `${DOMAIN}/${url}`,
      method: "POST",
      data: model,
      headers: configHeaders(), //JWT
    });
  };

  get = (url) => {
    return Axios({
      url: `${DOMAIN}/${url}`,
      method: "GET",
      headers: configHeaders(),
      //   headers: { Authorization: "Bearer " + localStorage.getItem(TOKEN) },
      //token yêu cầu từ backend chứng minh user đã đăng nhập rồi
    });
  };

  delete = (url) => {
    return Axios({
      url: `${DOMAIN}/${url}`,
      method: "DELETE",
      headers: configHeaders(), //token yêu cầu từ backend chứng minh user đã đăng nhập rồi
    });
  };
}
