import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { CarouselReducer } from "./reducers/CarouselReducer";
import { LoadingReducer } from "./reducers/LoadingReducer";
import { QuanLyNguoiDungReducer } from "./reducers/NguoiDungReducer";
import { QuanLyPhimReducer } from "./reducers/QuanLyPhimReducer";
import { QuanLyRapReducer } from "./reducers/QuanLyRapReducer";

const rootReducer = combineReducers({
  //state ung dung
  QuanLyPhimReducer: QuanLyPhimReducer,
  CarouselReducer: CarouselReducer,
  QuanLyRapReducer: QuanLyRapReducer,
  QuanLyNguoiDungReducer: QuanLyNguoiDungReducer,
  LoadingReducer: LoadingReducer,
});
export const store = createStore(rootReducer, applyMiddleware(thunk));
