import axios from "axios";
import { quanLyPhimService } from "../../services/QuanLyPhimService";

import { configHeaders, DOMAIN } from "../../util/Settings/config";
import { SET_CAROUSEL } from "./types/CarouselType";

export const getCarouselAction = () => {
  return async (dispatch) => {
    try {
      //sử dụng tham số thamSo
      const result = await quanLyPhimService.layDanhSachBanner();

      //   axios({
      //   url: `${DOMAIN}/api/QuanLyPhim/LayDanhSachBanner`,
      //   method: "GET",
      //   headers: configHeaders(),
      // });
      console.log("result", result);
      dispatch({
        type: SET_CAROUSEL,
        arrImg: result.data.content,
      });
    } catch (errors) {
      console.log("error", errors);
    }
  };
};
